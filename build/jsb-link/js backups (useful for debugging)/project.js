require = function s(r, i, a) {
function l(o, e) {
if (!i[o]) {
if (!r[o]) {
var c = "function" == typeof require && require;
if (!e && c) return c(o, !0);
if (u) return u(o, !0);
var n = new Error("Cannot find module '" + o + "'");
throw n.code = "MODULE_NOT_FOUND", n;
}
var t = i[o] = {
exports: {}
};
r[o][0].call(t.exports, function(e) {
return l(r[o][1][e] || e);
}, t, t.exports, s, r, i, a);
}
return i[o].exports;
}
for (var u = "function" == typeof require && require, e = 0; e < a.length; e++) l(a[e]);
return l;
}({
HelloWorld: [ function(e, o, c) {
"use strict";
cc._RF.push(o, "280c3rsZJJKnZ9RqbALVwtK", "HelloWorld");
cc.Class({
extends: cc.Component,
properties: {
text: "Hello, World!"
},
onLoad: function() {},
update: function(e) {}
});
cc._RF.pop();
}, {} ],
LoginWx: [ function(e, o, c) {
"use strict";
cc._RF.push(o, "5ce2flVxwtG6ol1jv8EljDa", "LoginWx");
cc.Class({
extends: cc.Component,
wechatLogin: function() {
jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity", "wxLogin", "()V");
this.getAccessTokenByCode();
},
getWechatCode: function() {
var c, n, t = this;
return new Promise(function(e, o) {
console.log("进入Promise");
t.schedule(function() {
c = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity", "getCodeSuccess", "()Z");
console.log("getWechatCode-isGetCode-is=", c);
if (c) {
console.log("-----ccclog------\x3e", c);
t.unscheduleAllCallbacks();
n = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity", "getCode", "()Ljava/lang/String;");
console.log("-----ccclog------\x3ecode is " + n);
e(n);
}
}.bind(t), .5);
});
},
getAccessTokenByCode: function() {
var o = this;
this.getWechatCode().then(function(e) {
console.log("-----ccclog------\x3e已经获得code");
console.log("-----ccclog------\x3ein AccessTokenByCode " + e);
o.getAccessToken(e);
});
},
getAccessToken: function(e) {
var o = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=wx528d25298f93f5a1&secret=2a6adca4f79fec4fb6caf4abb9c7ae8d&code=" + e + "&grant_type=authorization_code", s = this, r = new XMLHttpRequest();
r.onreadystatechange = function() {
if (4 == r.readyState && 200 <= r.status && r.status < 400) {
var e = r.responseText;
console.log("-----ccclog------\x3eresponse===>>>", e);
var o = JSON.parse(e), c = o.access_token, n = o.refresh_token, t = o.openid;
if (7200 <= o.expires_in) {
s.freshAccessToken(n).then(function(e) {
console.log("-----ccclog------\x3e刷新accessToken 是", e);
c = e;
s.getUserInfo(c, t);
cc.director.loadScene("helloworld");
});
console.log("-----ccclog------\x3e这个accessToken是刷新出来的token", c);
} else {
s.getUserInfo(c, t);
cc.director.loadScene("helloworld");
}
}
};
r.open("GET", o, !0);
r.send();
},
getUserInfo: function(e, o) {
console.log("-----ccclog------\x3eaccessToken is " + e);
console.log("-----ccclog------\x3eopenid is " + o);
var c = "https://api.weixin.qq.com/sns/userinfo?access_token=" + e + "&openid=" + o, n = new XMLHttpRequest();
n.onreadystatechange = function() {
if (4 == n.readyState && 200 <= n.status && n.status < 400) {
var e = n.responseText;
console.log("response===>>>", e);
var o = JSON.parse(e);
console.log("msg is ", o);
console.log("nickName is " + o.nickname);
console.log("city is " + o.city);
console.log("country " + o.country);
console.log("sex is  " + o.sex);
}
};
n.open("GET", c, !0);
n.send();
},
freshAccessToken: function(e) {
var n, o = "https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=wx528d25298f93f5a1&grant_type=refresh_token&refresh_token=" + e, t = new XMLHttpRequest();
return new Promise(function(c, e) {
t.onreadystatechange = function() {
if (4 == t.readyState && 200 <= t.status && t.status < 400) {
var e = t.responseText;
console.log("response===>>>", e);
var o = JSON.parse(e);
n = o.access_token;
console.log("ac is " + n);
c(n);
}
};
t.open("GET", o, !0);
t.send();
});
}
});
cc._RF.pop();
}, {} ]
}, {}, [ "HelloWorld", "LoginWx" ]);